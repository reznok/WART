from Target import Target


class BasePlugin:

    name = "NAME_NOT_SET"
    version = "VERSION_NOT_SET"

    def __init__(self, target: Target):
        """
        Takes in a target object

        :param target: Target target
        """
        self.target = target
        self.results = None

    def execute(self):
        self.update_target()

    def update_target(self):
        output_dict = {
            "name": self.name,
            "version": self.version,
            "results": self.results
        }
        self.target.add_plugin_data(output_dict)


