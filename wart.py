import json
from Target import Target
from plugins.WappalyzerPlugin import WappalyzerPlugin
from plugins.SkeletonPlugin import SkeletonPlugin
from plugins.ScreenshotPlugin import ScreenshotPlugin
from plugins.RobotsPlugin import RobotsPlugin
from plugins.SiteInfoPlugin import SiteInfoPlugin
import urllib3

urllib3.disable_warnings()

target_urls = ["http://example.com"]

results = []

for idx, url in enumerate(target_urls):
    print(f"Beginning Target: {url} ({idx + 1}/{len(target_urls)})")
    target = Target(url)
    try:
        WappalyzerPlugin(target).execute()
    except KeyboardInterrupt:
        print("Breaking...")
        break
    except Exception as e:
        print("Wappalyzer Fail")
    try:
        ScreenshotPlugin(target, "screenshots").execute()
    except:
        print("Screenshot Fail")
    try:
        SkeletonPlugin(target).execute()
    except:
        print("Skeleton Fail")

    try:
        RobotsPlugin(target).execute()
    except:
        print("Robots Fail")

    try:
        SiteInfoPlugin(target).execute()
    except:
        print("SiteInfo Fail")
    results.append(target.dict())


# print(json.dumps(results, indent=4))

f = open("example_output.json", "w")
f.write(json.dumps(results, indent=4))
f.close()
