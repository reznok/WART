## WART (Web Application Recon Tool)
  


```
pip install -r REQUIREMENTS
python wart.py
```

[Example Output](example_output.json)

## Plugins

#### Wappalyzer
Uses Wappalyzer(https://www.wappalyzer.com/) to determine what services are running on the target.  

Example Output:  
  
```
>>> wappalyzer.analyze("https://github.com")
{'GitHub Pages', 'Ruby', 'Bootstrap', 'jQuery', 'jQuery-pjax', 'Ruby on Rails'}
```

#### SiteInfo

Gets basic website information:
- HTML Title
- Response Headers

#### Screenshot
Takes a screenshot of the target. Depends on having either Chrome or Firefox installed. 

#### Robots
Grabs the target's robots.txt file if it exists.

#### Skeleton
Minimum functionality plugin to be used as boiler-plate for plugin creation